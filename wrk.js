
$(function() {
	
	data = Backbone.Model.extend({});

	
	dataCollection = Backbone.Collection.extend({
		model: data,

		
		comparator: function(item) {
			return item.get('pid');
		}
	});

	
	var ListView = Backbone.View.extend({
		el: $('#test'),
		ListTemplate: $("#tmpl").template(),
		render: function() {
			var self = this;
			this.el.fadeOut(0, function() {
				$('#List').empty();
				$.tmpl(self.ListTemplate, self.model.toArray()).appendTo(self.el.find('#List'));
				self.el.fadeIn(100);
			});
			return this;
		},
	});

	
	var datadetailsView = Backbone.View.extend({
		el: $('#datadetailsView'),
		
		datadetailsTemplate: $("#datadetailsTmpl").template(),
		render: function() {
			var self = this;
			this.el.fadeOut(0, function() {
				self.el.empty();
				$.tmpl(self.datadetailsTemplate, self.model).appendTo(self.el);
				self.el.fadeIn(100);
			});
			return this;
		},
		
	});

	
	var Application = Backbone.Controller.extend({
		_ListView: null,
		
		_datadetailsView: null,
		
		_tours: null,
		
		routes: {
			"": "List",
			"datadetails/:id": "datadetails",
		},

		
		initialize: function(options) {
			var self = this;
			if (this._ListView === null) {
				$.ajax({
					url: 'wrk.json',
					dataType: 'json',
					data: {},
					success: function(data) {
						
						self._tours = new dataCollection(data);
						self.List();
					}
				});
				return this;
			}
			return this;
		},

		List: function() {
			this._ListView = new ListView({
				model: this._tours
			});
			this._ListView.render();
		},

		datadetails: function(id) {
			this._datadetailsView = new datadetailsView({
				model: this._tours.at(id)
			});
			this._datadetailsView.render();
		}
	});

	
	app = new Application();
	Backbone.history.start();
});
